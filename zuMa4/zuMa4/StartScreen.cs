﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace zuMa4
{
    class StartScreen
    {
        Texture2D BackGround;
        public Game1 game;

        GraphicsDeviceManager graphicss;
        Button btnPlay;
        Button btnQuit;
        int screenWidth = 800, screenHeight = 480;
        Game1 gg;
        Song backGroundSong;
      
        public  StartScreen (Game1 game)
        {

            this.game = game;
            gg = new Game1();
            graphicss = gg.send();
            BackGround = game.Content.Load<Texture2D>("mainMenu");
            graphicss.PreferredBackBufferWidth = screenWidth;
            graphicss.PreferredBackBufferHeight = screenHeight;
            graphicss.ApplyChanges();
            btnPlay = new Button(game.Content.Load<Texture2D>("btnPlay"),graphicss.GraphicsDevice);
            btnPlay.setPosition(new Vector2(75, 280));

            btnQuit = new Button(game.Content.Load<Texture2D>("btnQuit"), graphicss.GraphicsDevice);
            btnQuit.setPosition(new Vector2 (75,350));

            backGroundSong = game.Content.Load<Song>("start1");
            PlayMusic(backGroundSong);
        }

        private void PlayMusic(Song song)
        {
            try
            {
                MediaPlayer.Play(song);
                MediaPlayer.IsRepeating = true;
            }
            catch
            { }
        }

        public void Update()
        {
            MouseState mouseState = Mouse.GetState();
            
            btnPlay.Update(mouseState);
            btnQuit.Update(mouseState);
            
            if(btnPlay.isClicked==true)
            {
                //game.StartGame();
                game.loading();
               //game.EndGame();
                
            }
            if(btnQuit.isClicked==true)
            {
               // game.Exit();
                game.FinishGame();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(BackGround, new Rectangle(0, 0, screenWidth, screenHeight), Color.White);
            btnPlay.draw(spriteBatch);
            btnQuit.draw(spriteBatch);
        }
    }
}
