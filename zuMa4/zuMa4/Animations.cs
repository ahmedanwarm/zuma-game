﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace zuMa4
{
    class Animations
    {
        Texture2D spriteStrip;
        float scale;
        // The time since we last updated the frame
        int elapsedTime;
        int FrameTime;
        int FrameCount;
        int currentFrame;
        Color color;
        Rectangle sourseRect = new Rectangle();
        Rectangle destinationRect = new Rectangle();
        public int FrameWidth;
        public int FrameHight;
        // The state of the Animation
        public bool Active;

        // Determines if the animation will keep playing or deactivate after one run
        public bool Looping;

        public Vector2 Position;

        public void Intialize(Texture2D texture, Vector2 position, int frameWidth, int frameHight, int framecount,
            int frameTime, Color color, float scale, bool looping)
        {
            this.color = color;
            this.FrameWidth = frameWidth;
            this.FrameHight = frameHight;
            this.FrameCount = framecount;
            this.FrameTime = frameTime;
            this.scale = scale;

            Looping = looping;
            Position = position;
            spriteStrip = texture;
            elapsedTime = 0;
            currentFrame = 0;

            Active = true;

        }

        public void Update(GameTime gameTime)
        {
            if (Active == false)
                return;
            elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (elapsedTime > FrameTime)
            {
                currentFrame++;
                if (currentFrame == FrameCount)
                {
                    currentFrame = 0;
                    if (Looping == false)
                    {
                        Active = false;
                    }
                }
                elapsedTime = 0;

            }
            sourseRect = new Rectangle(currentFrame * FrameWidth, 0, FrameWidth, FrameHight);
            destinationRect = new Rectangle((int)Position.X - (int)(FrameWidth * scale) / 2, (int)Position.Y - (int)(FrameHight * scale) / 2,
                (int)(FrameWidth * scale), (int)(FrameHight * scale));

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(spriteStrip, destinationRect, sourseRect, color);
        }
    }
}
