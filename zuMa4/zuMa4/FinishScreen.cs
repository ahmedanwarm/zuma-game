﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading;
using Microsoft.Xna.Framework.Media;

namespace zuMa4
{
    class FinishScreen
    {
        public Game1 game;
        Texture2D BG;
        GraphicsDeviceManager gra;
        int screenWidth = 800, screenHeight = 480;
        Song BGSong;
        int x;
        public FinishScreen(Game1 game)
        {
            this.game = game;
            gra = game.send();
            BG = game.Content.Load<Texture2D>("afterQuit - Copy");
            gra.PreferredBackBufferWidth = screenWidth;
            gra.PreferredBackBufferHeight = screenHeight;
            gra.ApplyChanges();
            BGSong = game.Content.Load<Song>("door");
            PlayMusic(BGSong);
            x = 0;
        }

        private void PlayMusic(Song song)
        {
            try
            {
                MediaPlayer.Play(song);
                MediaPlayer.IsRepeating = true;
            }
            catch
            { }
        }

        public void Update(GameTime gameTime)
        {

            x++;
            if (x == 320)
            {
                game.Exit();
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(BG, Vector2.Zero, Color.White);
        }
    }
}
