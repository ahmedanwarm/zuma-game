﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace zuMa4
{
    class Train_Node
    {
        public Animations NodeAnimation;
        //Texture2D insertNodeTexture;
        public Vector2 Position;
        public bool Active;
        public float  distance = 0;
        public int ID;

        public int width = 50;

        public int Height = 50;
        
        public float speed;


       
        
        public void Initialize(Animations animation, Vector2 position,int id)
        {
            NodeAnimation = animation;
            Position = position;
            Active = true;
            speed = 0.5f;
            ID = id;
            
            
        }



        public void Update(GameTime gameTime)
        {
            //Position.X -= speed;
            distance += speed;
            NodeAnimation.Position = Position;
            NodeAnimation.Update(gameTime);
           
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            NodeAnimation.Draw(spriteBatch);
        }
    }
}
