﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace zuMa4
{
    class GamePlayScreen
    {
        private Game1 game;
        Texture2D mainBG;
        Texture2D train_Node_texture;
        public List<Train_Node> Train = new List<Train_Node>();
        public Texture2D[] textures;
        TimeSpan SpawnTime;
        TimeSpan perviousSpawnTime;
        TimeSpan learnTime;
        TimeSpan First_learn;
        TimeSpan second_learn;
        TimeSpan third_learn_Time;
        TimeSpan Forth_learn_Time;


        bool First_learn_testure_visible = false;
        bool second_learn_texture_visible = false;
        bool thired_learn_texture_visible = false;
        bool forth_learn_texture_visible = false;

        Texture2D first_learn_tecture;
        Texture2D second_learn_texture;
        Texture2D third_learn_texture;
        Texture2D Forth_learn_texture;

        Train_Node Node;
        bool is_shot;
        Ball_Node new_ball = new Ball_Node();
        Ball_Node ball2 = new Ball_Node();
        Ball_Node ball3 = new Ball_Node();
        Ball_Node ball4 = new Ball_Node();
        Ball_Node ball5 = new Ball_Node();

        string s="b1" ;
        string ball_shot = "b1'";
        
        MouseState pastkey;//////for shut
       
        
        private float Rotation;
        private Texture2D player;
        private Vector2 origin;
        private Vector2 position;
        private Vector2 distance;//////////////position between the player and mouse
        /// bullets
        List<Bullet> byo = new List<Bullet>();

        GraphicsDeviceManager gra;

        int score;
        SpriteFont font;
        SoundEffect Bullet_Sound;
        SoundEffect intersectionSound;
        Song gamePlayMusic;

        Texture2D die;
        Animations dieAnimi;
        public GamePlayScreen(Game1 game)
        {
           
            this.game = game;
            //Train = new List<Train_Node>();
            perviousSpawnTime = TimeSpan.Zero;
            SpawnTime = TimeSpan.FromSeconds(1.75f);////////////// time between balls in train node
            learnTime = TimeSpan.FromSeconds(25f);
            First_learn = TimeSpan.FromSeconds(11f);
            second_learn = TimeSpan.FromSeconds(11.1f);
            third_learn_Time = TimeSpan.FromSeconds(15f);
            Forth_learn_Time = TimeSpan.FromSeconds(20f);
           

            textures = new Texture2D[6];
            is_shot = false;
           

            mainBG = game.Content.Load<Texture2D>("bkk");///////////////intial value (ely el train htbd2 beh daymn)
            textures[0] = game.Content.Load<Texture2D>("66");
            textures[1] = game.Content.Load<Texture2D>("33");
            textures[2] = game.Content.Load<Texture2D>("22");
            textures[3] = game.Content.Load<Texture2D>("11");
            textures[4] = game.Content.Load<Texture2D>("44");
            textures[5] = game.Content.Load<Texture2D>("55");
     
            new_ball.Width = 200;
            new_ball.Hight = 500;
        
            ball2.Width = 800;
            ball2.Hight = 80;

            ball3.Width = 30;
            ball3.Hight = 600;
             
            ball4.Width = 800;
            ball4.Hight = 100;

            ball5.Width = 200;
            ball5.Hight = 60;
                /////////////////////////////// 34an el direction bta3 el path mt2sm 3la 4 agza2
            new_ball.Position = new Vector2(750, 100);

            ball2.Position = new Vector2(400, 30);

            ball3.Position = new Vector2(30, 220);

            ball4.Position = new Vector2(300, 500);

            ball5.Position = new Vector2(750, 500);

            gra = game.send();

            player = game.Content.Load<Texture2D>("Anim-1");
            Viewport viewport = gra.GraphicsDevice.Viewport;
            position.X = viewport.Width / 2;
            position.Y = viewport.Height / 2;
            origin.X = player.Width / 2;
            origin.Y = player.Height / 2;


             score = 0;
             font = game.Content.Load<SpriteFont>("gameFont");

             gamePlayMusic = game.Content.Load<Song>("menuMusic");
             Bullet_Sound = game.Content.Load<SoundEffect>("195396__goup-1__boom-kick");
             intersectionSound = game.Content.Load<SoundEffect>("tuk");

             PlayMusic(gamePlayMusic);

             die = game.Content.Load<Texture2D>("Animation-wa7sh");

             dieAnimi = new Animations();
            dieAnimi.Intialize(die, new Vector2(650, 420), 110, 149, 4, 100, Color.White, 1f, true);

            first_learn_tecture = game.Content.Load<Texture2D>("2_instruction");
            second_learn_texture = game.Content.Load<Texture2D>("3_instruction");
            third_learn_texture = game.Content.Load<Texture2D>("1_instruction");
            Forth_learn_texture = game.Content.Load<Texture2D>("4_instruction");

        } //loading content and Intializing values of variables.

        private void PlayMusic(Song song)
        {
            try
            {
                MediaPlayer.Play(song);
                MediaPlayer.IsRepeating = true;
            }
            catch
            { }
        } // playing the soundtrack throw the game.

      

        private void addTrain()
        {
            Node = new Train_Node();

            Random chooseID = new Random();

            int train_Node_ID = chooseID.Next(0, 6);
            train_Node_texture = textures[train_Node_ID];

            Animations nodeAnimation = new Animations();
            nodeAnimation.Intialize(train_Node_texture, Vector2.Zero, 50, 50, 4, 100, Color.White, 1f, true);

            Vector2 position = new Vector2(800, 220);
           
            Node.Initialize(nodeAnimation, position, train_Node_ID);
            Train.Add(Node);
        } // adding new noda in the Train list


        private void UpdateTrain(GameTime gameTime)
        {
            if (gameTime.TotalGameTime - perviousSpawnTime > SpawnTime)
            {
                
                    perviousSpawnTime = gameTime.TotalGameTime;
                    addTrain();
                
            }

            for (int i = Train.Count - 1; i >= 0; i--)
            {
                Train[i].Update(gameTime);
                
            }
        }  // updating Train 

        private void Direction()
        {
            Rectangle hideObject;
            Rectangle trainPosition;
            Rectangle hideObject2;
            Rectangle hideObject3;
            Rectangle hideObject4;
            Rectangle hideObject5;

            hideObject = new Rectangle((int)new_ball.Position.X - new_ball.Width / 2, (int)new_ball.Position.Y - new_ball.Hight / 2,
                new_ball.Width, new_ball.Hight);
            foreach (Train_Node node in Train)
            {
                trainPosition = new Rectangle((int)node.Position.X - node.width / 2, (int)node.Position.Y - node.Height / 2,
                    node.width, node.Height);
                if (trainPosition.Intersects(hideObject))
                {
                    node.Position.Y -= (node.speed) * 2 / 3;
                    node.Position.X -= node.speed * 2 / 3;
                }

            }
            hideObject2 = new Rectangle((int)ball2.Position.X - ball2.Width / 2, (int)ball2.Position.Y - ball2.Hight / 2,
                ball2.Width, ball2.Hight);
            foreach (Train_Node node in Train)
            {
                trainPosition = new Rectangle((int)node.Position.X - node.width / 2, (int)node.Position.Y - node.Height / 2,
                    node.width, node.Height);
                if (trainPosition.Intersects(hideObject2))
                {
                    node.Position.X -= node.speed;
                   
                }

            }



            hideObject3 = new Rectangle((int)ball3.Position.X - ball3.Width / 2, (int)ball3.Position.Y - ball3.Hight / 2,
                ball3.Width, ball3.Hight);
            foreach (Train_Node node in Train)
            {
                trainPosition = new Rectangle((int)node.Position.X - node.width / 2, (int)node.Position.Y - node.Height / 2,
                    node.width, node.Height);
                if (trainPosition.Intersects(hideObject3))
                {
                    node.Position.Y += node.speed;

                }

            }


            hideObject4 = new Rectangle((int)ball4.Position.X - ball4.Width / 2, (int)ball4.Position.Y - ball4.Hight / 2,
                ball4.Width, ball4.Hight);
            foreach (Train_Node node in Train)
            {
                trainPosition = new Rectangle((int)node.Position.X - node.width / 2, (int)node.Position.Y - node.Height / 2,
                    node.width, node.Height);
                if (trainPosition.Intersects(hideObject4))
                {

                    node.Position.X += node.speed;
                }
                
            }

            hideObject5 = new Rectangle((int)ball5.Position.X - ball5.Width / 2, (int)ball5.Position.Y - ball5.Hight / 2,
                ball5.Width, ball5.Hight);

          


            for (int i = 0; i < Train.Count - 1; i++)
            {
                trainPosition = new Rectangle((int)Train[i].Position.X /*- Train[i].width / 2*/
                    , (int)Train[i].Position.Y/* - Train[i].Height / 2*/,
                    Train[i].width, Train[i].Height);

                if (trainPosition.Intersects(hideObject5))
                {
                    // Train.RemoveAt(i);
                    game.EndGame();
                }
            }



        }   // making the path of the Train

        public void Update_bullet()
        {

            foreach (Bullet built in byo)/////////// tol ma feh object mn class bult fe el byo list 
            {
                built.position += built.velocity;
                if (Vector2.Distance(built.position, position/*player position*/) > 400)/////////// 34an ama el msafa tkbr y5feh mn el 4a4a
                    built.is_visible = false;
            }
            for (int i = byo.Count - 1; i > 0; i--)
            {
                try
                {
                    if (byo[i].is_visible == false)
                    {
                        byo.RemoveAt(i);
                        i++;
                    }
                }
                catch
                {
                    return;
                }
            }

        }// to update the Bullet shot.

        public void shoot()
        {
            

            Bullet newbullet = new Bullet(game.Content.Load<Texture2D>(s));
            switch (s)
            {
                case "b1":
                    newbullet.pushID(3);
                    break;
                case "b2":
                    newbullet.pushID(2);
                    break;
                case "b3":
                    newbullet.pushID(1);
                    break;
                case "b4":
                    newbullet.pushID(4);
                    break;
                case "b5":
                    newbullet.pushID(5);
                    break;
                case "b6":
                    newbullet.pushID(0);
                    break;
            }
            newbullet.velocity = new Vector2((float)Math.Sin(Rotation),
                -(float)Math.Cos(Rotation))/*34an azbtha m3 el rotation bta3 el player*/ * 7f;
            newbullet.position = new Vector2(position.X - 30, position.Y - 30)/*34an azbtha fe el nos belzbt*/;
            newbullet.is_visible = true;
            if (byo.Count() < 50)
                byo.Add(newbullet);

            Bullet_Sound.Play();
        } //to make a new bullet and Fire it.

       
        public void check(int position,int id)
        {
            if (Train[position].ID == id)
            {
                Train.RemoveAt(position);
                score += 10;
            }
            else
            {
                Train[position].speed += 0.2f;
            }
        }

        public void check_level()
        {
            if (score >= 70 && score < 71)
            {
                for (int i = 0; i < Train.Count - 1; i++)
                {
                    Train[i].speed = 0.7f;
                }
                SpawnTime = TimeSpan.FromSeconds(1.7);

            }
            else if (score >= 200 && score < 201) 
            {
                for (int i = 0; i < Train.Count - 1; i++)
                {
                    Train[i].speed = 1f;
                }
                SpawnTime = TimeSpan.FromSeconds(1.5f);

            }
        }


        public void Intersection()
        {

            Rectangle bullet;
            Rectangle trainObject;


            for (int i = 0; i < Train.Count; i++)
            {
                for (int j = 0; j < byo.Count; j++)
                {
                    bullet = new Rectangle((int)byo[j].position.X-byo[j].width/2,(int)byo[j].position.Y-byo[j].height/2, 33, 33);
                    
                    trainObject = new Rectangle((int)Train[i].Position.X-Train[i].width ,
                        (int)Train[i].Position.Y-Train[i].Height, Train[i].width-10, Train[i].Height-10);

                    if (bullet.Intersects(trainObject))
                    {
                        
                        byo[j].is_visible = false;
                        int iD = byo[j].GetID();
                        check(i, iD);
                        return;
                    }
                }
            }
        } // to know if there was intersection between the bullet and our Train.

        public void Update(GameTime gameTime)
        {

            UpdateTrain(gameTime);
            Direction();
            
            MouseState mouseState = Mouse.GetState();///// for shut
            distance.X = mouseState.X - position.X;
            distance.Y = mouseState.Y - position.Y;
            Rotation = (float)Math.Atan2((double)distance.Y,///////math.atan2 ->Returns the angle whose tangent is the quotient of two specific numbers.
           (double)distance.X) + MathHelper.PiOver2;//the direction between the position of the sprite and the position of the mouse. 
            //MathHelper.PiOver2-> Represents the value of pi divided by two.
            ////rotaion
            MouseState mouseStatee = Mouse.GetState();/// for change balls//////
            if (mouseState.LeftButton == ButtonState.Pressed && pastkey.LeftButton == ButtonState.Released/*bdmn en el space m4 mtdas 3leha*/)
            {
                shoot();
                Random e5tar = new Random();
                int chooses = e5tar.Next(0, 6);

                if (chooses == 0)
                {
                    s = "b2";
                    ball_shot = "b2'";
                }
                else if (chooses == 1)
                {
                    s = "b3";
                    ball_shot = "b3'";
                }

                else if (chooses == 2)
                {
                    s = "b4";
                    ball_shot = "b4'";
                }

                else if (chooses == 3)
                {
                    s = "b5";
                    ball_shot = "b5'";
                }

                else if (chooses == 4)
                {
                    s = "b6";
                    ball_shot = "b6'";
                }
                else
                {
                    s = "b1";
                    ball_shot = "b1'";
                }
                is_shot = true;

            }
            
            pastkey = Mouse.GetState();

            Update_bullet();

            Intersection();
            dieAnimi.Update(gameTime);
            if (gameTime.TotalGameTime < learnTime)
            {
                if (gameTime.TotalGameTime < First_learn)
                {
                    First_learn_testure_visible = true;
                }
                else if (gameTime.TotalGameTime > First_learn && gameTime.TotalGameTime < second_learn)
                {
                    First_learn_testure_visible = false;
                    second_learn_texture_visible = true;
                }
                else if (gameTime.TotalGameTime > third_learn_Time && gameTime.TotalGameTime < Forth_learn_Time)
                {
                    second_learn_texture_visible = false;
                    thired_learn_texture_visible = true;
                }

                else if (gameTime.TotalGameTime > Forth_learn_Time)
                {
                    thired_learn_texture_visible = false;
                    forth_learn_texture_visible = true;
                }
                
            }
            else
            {
                forth_learn_texture_visible = false;
            }
            check_level();
            game.getScore(score);

        } // Updating out game

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(mainBG, Vector2.Zero, Color.White);
           
            for (int i = 0; i < Train.Count; i++)
            {
                Train[i].Draw(spriteBatch);
            }

            foreach (Bullet built in byo)
            {
                built.draw(spriteBatch);
            }
            spriteBatch.Draw(player, position, null/*rectangle of el pic*/, Color.White, Rotation,
                     origin/*origin to rotate around it */, 1.0f/*scale the player*/, SpriteEffects.None, 0f/*layer depth*/);
            if (is_shot)
            {

                spriteBatch.Draw(game.Content.Load<Texture2D>("Anim-2"), position, null/*rectangle of el pic*/, Color.White, Rotation,
                         origin/*origin to rotate around it */, 1.0f/*scale the player*/, SpriteEffects.None, 0f/*layer depth*/);

                Thread.Sleep(50);

                spriteBatch.Draw(game.Content.Load<Texture2D>("Anim-3"), position, null/*rectangle of el pic*/, Color.White, Rotation,
                         origin/*origin to rotate around it */, 1.0f/*scale the player*/, SpriteEffects.None, 0f/*layer depth*/);

                is_shot = false;
            }
            spriteBatch.Draw(game.Content.Load<Texture2D>(ball_shot), new Vector2(position.X - 10, position.Y - 10), Color.White);

            spriteBatch.DrawString(font, "Score : " + score, Vector2.Zero, Color.LemonChiffon);

          //  spriteBatch.Draw(die, new Vector2(620, 350), Color.White);

            dieAnimi.Draw(spriteBatch);

            if(First_learn_testure_visible==true)
            {
                spriteBatch.Draw(first_learn_tecture, new Vector2(550, 0), Color.White);
                
            }
            if(second_learn_texture_visible)
            {
                spriteBatch.Draw(second_learn_texture, new Vector2(200, 100), Color.White);
            }
            if(thired_learn_texture_visible)
            {
                spriteBatch.Draw(third_learn_texture, new Vector2(0, 0), Color.White);
            }
            if(forth_learn_texture_visible)
            {
                spriteBatch.Draw(Forth_learn_texture, new Vector2(100, 70), Color.White);

            }
        }//Drawingour game.
    }
}
