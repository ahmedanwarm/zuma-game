using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace zuMa4
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        enum Screen
        {
            StartScreen,
            loadingScreen,
            GamePlayScreen,
            GameOverScreen,
            FinishScreen
        }

        GraphicsDeviceManager graphics;///////// 34an hb3tha b send l classes tanya 
        SpriteBatch spriteBatch;

        StartScreen startScreen;
        loadingScreen LoadingScreen;
        GamePlayScreen gamePlayScreen;
        GameOverScreen gameOverScreen;
        FinishScreen finishScreen;
        int Score;

        Screen currentScreen;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        public GraphicsDeviceManager send()
        {
            return graphics;
        }

        
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
           
            base.Initialize();
        }

       
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
           
            startScreen = new StartScreen(this);
            currentScreen = Screen.StartScreen;
           
            // TODO: use this.Content to load your game content here
        }

       
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            IsMouseVisible = true;
            switch (currentScreen)
            {
                case Screen.StartScreen:
                    if (startScreen != null)
                    { 
                        startScreen.Update();
                        
                    }

                    break;
                case Screen.loadingScreen:
                    if(LoadingScreen!=null)
                    {
                        LoadingScreen.Update(gameTime);
                    }
                    break;

                case Screen.GamePlayScreen:
                    if (gamePlayScreen != null)
                        gamePlayScreen.Update(gameTime);
                    break;
                case Screen.GameOverScreen:
                    gameOverScreen.Update(gameTime);
                   break;
                case Screen.FinishScreen:
                    if(finishScreen!=null)
                    {
                        finishScreen.Update(gameTime);
                    }
                    break;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();


            switch (currentScreen)
            {
                case Screen.StartScreen:
                    if (startScreen != null)
                        startScreen.Draw(spriteBatch);
                    break;
                case Screen.loadingScreen:
                    if(LoadingScreen!=null)
                    {
                        LoadingScreen.Draw(spriteBatch);
                    }
                    break;
                case Screen.GamePlayScreen:
                    if (gamePlayScreen != null)
                        gamePlayScreen.Draw(spriteBatch);
                    break;
                case Screen.GameOverScreen:
                    gameOverScreen.Draw(spriteBatch);
                    break;
                case Screen.FinishScreen:
                    if (finishScreen != null)
                    {
                        finishScreen.Draw(spriteBatch);
                    }
                    break;
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public void begin()
        {
            startScreen = new StartScreen(this);
            currentScreen = Screen.StartScreen;
            gameOverScreen = null;
        }
        public void StartGame()
        {
            gamePlayScreen = new GamePlayScreen(this);
            currentScreen = Screen.GamePlayScreen;
            startScreen = null;
        }
        public void EndGame()
        {

            gameOverScreen = new GameOverScreen(this);
            currentScreen = Screen.GameOverScreen;
            gamePlayScreen = null;

        }
        public void loading()
        {
            LoadingScreen = new loadingScreen(this);
            currentScreen = Screen.loadingScreen;
            startScreen = null;
        }
        public void FinishGame()
        {
            finishScreen = new FinishScreen(this);
            currentScreen = Screen.FinishScreen;
            gameOverScreen = null;
        }
        public void getScore(int x)
        {
            Score = x;
        }
        public int sendScore()
        {
            return Score;
        }
    }
}
