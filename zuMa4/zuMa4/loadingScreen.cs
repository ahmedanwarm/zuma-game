﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;


namespace zuMa4
{
    class loadingScreen
    {
        public Game1 game1;
        int i = 0;
        Song loadingMusic;
        ScrollingBackground myBackGround;

        public loadingScreen(Game1 game)
        {
            this.game1 = game;

          
            loadingMusic = game.Content.Load<Song>("loading");
            PlayMusic(loadingMusic);

            myBackGround = new ScrollingBackground();
            Texture2D background = game.Content.Load<Texture2D>("loadingAnimation");
            myBackGround.Load(game1.GraphicsDevice, background);
        }

        private void PlayMusic(Song song)
        {
            try
            {
                MediaPlayer.Play(song);
                //MediaPlayer.IsRepeating = true;
            }
            catch
            { }
        }

        public void Update(GameTime gameTime) 
        {
            i++;
            
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            myBackGround.Update(elapsed * 100);
            if (i == 340)
            {
                i = 0;
                game1.StartGame();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
           // spriteBatch.Draw(BG, Vector2.Zero, Color.White);
            myBackGround.Draw(spriteBatch);

        }
    }
}
