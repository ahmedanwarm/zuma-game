﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace zuMa4
{
    class Bullet
    {
        public Texture2D shut;
        public Vector2 position;
        public Vector2 velocity;
        public Vector2 origin;
        public bool is_visible;
        public Bullet(Texture2D t)
        {
            shut = t;
            is_visible = true;

        }
        public int ID;
        public int width = 43;

        public int height = 43;
       

        public int GetID()
        {
            return ID;
        }

        public void pushID(int id)
        {
            ID = id;
        }
        public void draw(SpriteBatch spritbatch)
        {
            spritbatch.Draw(shut, position, null, Color.White, 0f, origin, 1f, SpriteEffects.None, 0);
        }
    }
}
