﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading;
using Microsoft.Xna.Framework.Media;

namespace zuMa4
{
    class GameOverScreen
    {
        public Game1 game;
        Texture2D[] animi;
        Texture2D BG;
        int i = 0;
        int j = 0;
        int yourScore;
        Song BGSong;
        SpriteFont font;


        public GameOverScreen(Game1 game)
        {
            this.game = game;
            animi = new Texture2D[10];
            animi[0]=game.Content.Load<Texture2D>("0");
             animi[1]=game.Content.Load<Texture2D>("1");
             animi[2]=game.Content.Load<Texture2D>("2");
             animi[3]=game.Content.Load<Texture2D>("3"); 
             animi[4]=game.Content.Load<Texture2D>("4");
             animi[5]=game.Content.Load<Texture2D>("5");
             animi[6]=game.Content.Load<Texture2D>("6");
             animi[7]=game.Content.Load<Texture2D>("7");
             animi[8]=game.Content.Load<Texture2D>("8");
             animi[9]=game.Content.Load<Texture2D>("9");
             BG = animi[0];
             BGSong = game.Content.Load<Song>("End");
             PlayMusic(BGSong);
             yourScore = game.sendScore();
             font = game.Content.Load<SpriteFont>("gameFont");

        }


        private void PlayMusic(Song song)
        {
            try
            {
                MediaPlayer.Play(song);
               // MediaPlayer.IsRepeating = true;
            }
            catch
            { }
        }

        public void Update(GameTime gameTime)
        {
           
            j++;
            if (j < 15) 
            {
                BG = animi[0];
            }
            if (j >= 30 && j < 45) 
            {
                BG = animi[1];
            }
            if (j >= 60 && j < 75)
            {
                BG = animi[2];
            }
            if (j >= 90 && j < 115) 
            {
                BG = animi[3];
            }
            if (j >= 115 && j < 130) 
            {
                BG = animi[4];
            }
            if (j >= 130 && j < 145) 
            {
                BG = animi[5];
            }
            if (j >= 145 && j < 160) 
            {
                BG = animi[6];
            }
            if (j >= 160 && j < 175) 
            {
                BG = animi[8];
            }
            if (j >= 175 && j < 190) 
            {
                BG = animi[9];

            }
            if(j>190)
            {
                Thread.Sleep(1000);
                game.begin();
            }
         
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //animation.Draw(spriteBatch);
           
            
                spriteBatch.Draw(BG, Vector2.Zero, Color.White);
                spriteBatch.DrawString(font, "Score : " + yourScore, Vector2.Zero, Color.LemonChiffon);

        }
    }
}
