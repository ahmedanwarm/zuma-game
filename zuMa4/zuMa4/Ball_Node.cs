﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace zuMa4
{
    class Ball_Node
    {
        public Texture2D ball_texture;
        public Vector2 Position;
        public bool Active;

        public Animations ball_animation;

        public int Width = 20;
       
        public int Hight = 100;
       

        public void Initialize(Animations animation, Vector2 position)
        {

            ball_animation = animation;
            Position = position;
            Active = true;

        }


        public void Update(GameTime gameTime)
        {
            
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ball_texture, Position, Color.White);
        }

    }
}
